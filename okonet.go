package okonet

import (
	"encoding/gob"
	"fmt"

	//"encoding/base64"
	"net"

	"gitlab.com/ashinnv/okoframe"
	//"okoframe"

	"gocv.io/x/gocv"
)

/*
TODO: Senders need to handle errors
*/

func FrameListen(listenTarget string, output chan okoframe.Frame) {

	ln, err := net.Listen(listenTarget, "tcp")
	var tmpFrame okoframe.Frame
	if err != nil {
		fmt.Println("ERROR: ", err)
	}

	for {
		conn, err := ln.Accept()
		if err != nil {
			fmt.Println("ERROR: ", err)
		}

		dec := gob.NewDecoder(conn)
		err = dec.Decode(&tmpFrame)

		if err != nil {
			fmt.Println("ERROR: ", err)
		}

		output <- tmpFrame

	}
}

func FrameSender(sendTarget string, input chan okoframe.Frame) {
	con, err := net.Dial(sendTarget, "tcp")
	if err != nil {
		fmt.Println("ERROR: ", err)
		//fmt.Println("asdf")
		return
	}

	defer con.Close()

	for snd := range input {

		enc := gob.NewEncoder(con)
		err := enc.Encode(snd)
		if err != nil {
			fmt.Println("ERROR: ", err)
		}

	}
}

func MatListen(listenTarget string, output chan gocv.Mat) {
	ln, err := net.Listen(listenTarget, "tcp")
	var tmpMat gocv.Mat
	if err != nil {
		fmt.Println("ERROR: ", err)
	}

	for {
		conn, err := ln.Accept()
		if err != nil {
			fmt.Println("ERROR: ", err)
		}

		dec := gob.NewDecoder(conn)
		err = dec.Decode(&tmpMat)

		if err != nil {
			fmt.Println("ERROR: ", err)
		}

		output <- tmpMat

	}
}

func MatSender(sendTarget string, input chan gocv.Mat) {
	con, err := net.Dial(sendTarget, "tcp")
	if err != nil {
		fmt.Println("ERROR: ", err)
	}

	defer con.Close()

	for snd := range input {

		enc := gob.NewEncoder(con)
		err := enc.Encode(snd)
		if err != nil {
			fmt.Println("ERROR: ", err)
		}

	}
}

/*
func FrameListenBase64(listenTarget string, output chan okoframe.Frame){

	var tmpFrame okoframe.Frame
	ln, err := net.Listen(listenTarget, "tcp")
	if err != nil{
		fmt.Println(err)
	}

	for{
		conn, err := ln.Accept()
		if err != nil{
			fmt.Println("Error FLB func:", err)
		}

		dec := base64.NewDecoder(base64.)


	}

}
*/
